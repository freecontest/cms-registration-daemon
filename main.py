#!/usr/bin/env python3
import json
import logging

from flask import Flask, render_template, request

import cms_register
import utils

logger = logging.getLogger(__name__)

app = Flask(__name__)
app.secret_key = utils.generate_random_string(128)

@app.route("/", methods = ["GET"])
def index():
    return render_template("index.html")

@app.route("/register", methods = ["POST"])
def register():
    REQUIRED_FIELDS = {
        "username": str,
        "password": str,
        "first_name": str,
        "last_name": str,
        "contest_id": int,
        "hidden": int
    }

    logger.debug(f"Original form data {request.form}")

    sent_secret_key = request.form.get("secret_key")
    if sent_secret_key != app.config["AUTH_KEY"]:
        return respond(False, "invalid or missing secret_key", 403)

    params = {}
    for (field_name, expected_type) in REQUIRED_FIELDS.items():
        try:
            value = request.form[field_name]
            converted_value = expected_type(value)
        except (ValueError, KeyError):
            return respond(False, f"invalid or missing field: {field_name}")
        else:
            params[field_name] = converted_value

    params["hidden"] = bool(params["hidden"])

    logger.debug(f"Parsed form data: {params}")
    success, err = cms_register.register(**params)
    return respond(success, err)

def respond(success: bool, err: str, status: int = None):
    response = {
        "success": success,
        "err": err
    }

    if status == None:
        status = 200 if success else 401

    return (json.dumps(response), status)

if __name__ == "__main__":
    config = utils.get_config()
    app.config["AUTH_KEY"] = config["auth_key"]

    app.run(host = config["host"], port = config["port"])
