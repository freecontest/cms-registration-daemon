import configparser
import random
import string
import sys

def generate_random_string(length: int = 50) -> str:
    rand = random.SystemRandom()
    letters = string.ascii_letters + string.digits
    return ''.join(rand.choice(letters) for _ in range(length))

def get_or_exit(obj, name):
    try:
        return obj[name]
    except KeyError:
        logger.error(f"key {name} does not exist, bailing")
        sys.exit(1)

def get_config():
    config = configparser.ConfigParser()
    config.read("config.ini")

    general_section = get_or_exit(config, "general")

    host = get_or_exit(general_section, "host")
    port = get_or_exit(general_section, "port")
    auth_key = get_or_exit(general_section, "auth_key")

    try:
        port = int(port)
    except ValueError:
        logger.error("'port' is not an integer")
        sys.exit(1)

    return {
        "host": host,
        "port": port,
        "auth_key": auth_key
    }
