#!/usr/bin/env python3

# We enable monkey patching to make many libraries gevent-friendly
# (for instance, urllib3, used by requests)
import gevent.monkey
gevent.monkey.patch_all()  # noqa

import logging

from sqlalchemy.exc import IntegrityError

from cms import utf8_decoder
from cms.db import Contest, Participation, SessionGen, User

from cmscommon.crypto import build_password

def register(first_name, last_name, username, password, contest_id, hidden):
    stored_password = build_password(password)

    with SessionGen() as session:
        user = User(first_name = first_name,
                    last_name = last_name,
                    username = username,
                    password = stored_password)

        contest = Contest.get_from_id(contest_id, session)
        if contest is None:
            return (False, "Contest id not found")
            
        participation = Participation(hidden = hidden, 
                                      contest = contest,
                                      user = user)
        try:
            session.add(user)
            session.add(participation)
            session.commit()
        except IntegrityError:
            return (False, "user already exists")

    return (True, None)
